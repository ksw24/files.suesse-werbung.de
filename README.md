# files.suesse-werbung.de
Jump directly to:  
- [Introduction](#introduction)  
- [Usage](#usage)  
- [Naming Conventions](#naming-conventions)  
- [Examples](#examples)  
- [Automated Changelog](#automated-changelog)  
- [Contact](#contact)

___
## Introduction

Starting from 2022 on, we are building a new home for our product and production related files like datasheets, stand drawings and more.

Our goal is to create services which can be accessed and consumed by highly automated systems. 

As we ourselves are more and more relying on systems like this, it's only fair to offer the same level of service, too.
  
___
## Usage

We have set up internal workflows to provide files under the basepath `https://files.suesse-werbung.de`. 

Subfolders will typically point to article numbers, which are in the format of `11[0-9]{7}`, e.g. "110101050". So, the folder for the article with SKU 110101050 can be accessed under `https://files.suesse-werbung.de/110101050/`. 

You can obtain article numbers either from our "master tables" or from our website. We'll also use them in any conversation regarding product specific requests with you.

If you perform a `GET` request against this path, you'll receive a manifest-like listing of all the files (and folders) in the specified location.

By utilizing that information it should be possible to automatically grab the necessary files and information per article for further use in your systems.

By default the data will be returned as human-readable HTML. If you want to receive the data in XML format, append `?asXML` as a parameter to the request path, JSON can be requested by appending `?asJSON` to the URL.

Currently no authentification is required. Please use our resources responsibly. Thanks.
  
___
## Naming conventions

For explaining the naming conventions, we will stick to the article number `110101050` as used before.

You will find probably at least the first three types of files in any folder you request. Filename obviously depends on the subfolder you requested.

| Filename      | Description |
| ------------- | ----------- |
| 110101050.pdf | Stand drawing in PDF format, this can be used to visualize the placement of artwork, crease and cut lines, non-printable areas and more |
| 110101050_H.pdf | same as above but specified as a variant for portrait layout, e.g. for advent calendars |
| 110101050_Q.pdf | same as above but specified as a variant for landscape layout, e.g. for advent calendars |
| 110101050.eps | Stand drawing in EPS format |
| PS_110101050.pdf | Product specification, contains information on ingredients, allergenes and more |
| 110101050_OVERLAY.pdf | PDF with placeholders for overlays. These files simulate where our workflows for "online products" will place mandatory information. Also contains cut/crease lines for a better understanding of the product. |
| 110101050.jpg | Main image for this product |
| 110101050_RS.jpg | Image of the backside of this product |
  
___
## Examples
  
### Request (JSON)
`> GET https://files.suesse-werbung.de/110101050/?asJSON`

### Reply
```json
{
	"comment": "Not sure what you are looking at? Check this: https:\/\/gitlab.com\/ksw24\/files.suesse-werbung.de",
	"filecount": 3,
	"basepath": "https:\/\/files.suesse-werbung.de\/110101050\/",
	"files": [
		{
			"name": "110101050.eps",
			"extension": "eps",
			"type": "image\/x-eps",
			"size": 1365482
		},
		{
			"name": "110101050.pdf",
			"extension": "pdf",
			"type": "application\/pdf",
			"size": 463763
		},
		{
			"name": "PS_110101050.pdf",
			"extension": "pdf",
			"type": "application\/pdf",
			"size": 106117
		}
	]
}
```

### Request (XML)
`> GET https://files.suesse-werbung.de/110101050/?asXML`

### Reply
```xml
<?xml version="1.0"?>
<manifest>
	<comment>Not sure what you are looking at? Check this: https://gitlab.com/ksw24/files.suesse-werbung.de</comment>
	<filecount>3</filecount>
	<basepath>https://files.suesse-werbung.de/110101050/</basepath>
	<files>
		<file extension="eps" type="image/x-eps" size="1365482">110101050.eps</file>
		<file extension="pdf" type="application/pdf" size="463763">110101050.pdf</file>
		<file extension="pdf" type="application/pdf" size="106117">PS_110101050.pdf</file>
	</files>
</manifest>
```
___
## Automated Changelog
  
At 02:00 each night, two changelog files will be build. These files will contain changes made in the last two weeks (`strtotime(today -2 weeks)`) to the files.

You can use these changelog files to automatically update your systems, too. We do currently use the two keywords `CREATED` or `UPDATED`, depending on what kind of action was performed for the file.

### Structure (XML)
```xml
<?xml version="1.0"?>
<changelog>
	<change>
		<article>110473302</article>
		<file>110473302_OVERLAY.pdf</file>
		<action>CREATED</action>
		<date>2022-03-29 15:10:27</date>
	</change>
</changelog>
```

### Structure (JSON)
```JSON
[{
	"article":"110473302",
	"file":"110473302_OVERLAY.pdf",
	"action":"CREATED",
	"date":"2022-03-29 15:10:27"
}]
```

### Changelog files:  
[changelog.xml](https://files.suesse-werbung.de/changelog.xml)  
[changelog.json](https://files.suesse-werbung.de/changelog.json)  
  
___
## Contact
For any questions, please contact us via support@suesse-werbung.de only. 

Mails to this address will land in our Freshdesk, where we have lots of eyes on them quickly.
